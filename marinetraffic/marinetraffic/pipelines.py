# -*- coding: utf-8 -*-
import logging
import pymongo


class MongoPipeline(object):
    collection_name = "ships"

    def __init__(self, mongo_uri, mongo_db):
        self.mongo_uri = mongo_uri
        self.mongo_db = mongo_db

    @classmethod
    def from_crawler(cls, crawler):
        ## pull in information from settings.py
        return cls(
            mongo_uri=crawler.settings.get('MONGO_URI'),
            mongo_db=crawler.settings.get('MONGO_DATABASE'),

        )

    def same_product_check(self, item):
        product_results = self.db[self.collection_name].find({'sku': item["sku"], 'source': item["source"]}).count()
        return True if product_results > 0 else False

    def update_product(self, item):
        update_product = self.db[self.collection_name].replace_one({'sku': item["sku"], 'source': item["source"]}, item)

    def open_spider(self, spider):
        ## initializing spider
        ## opening db connection
        self.client = pymongo.MongoClient(self.mongo_uri)
        self.db = self.client[self.mongo_db]

    def close_spider(self, spider):
        ## clean up when spider is closed
        self.client.close()

    def process_item(self, item, spider):
        # # if self.same_product_check(item):
        # #     self.update_product(item)
        # # else:
        # #     self.db[self.collection_name].insert(dict(item))
        # self.db[self.collection_name].insert(dict(item))
        # logging.debug("Post added to MongoDB")
        self.db[self.collection_name].replace_one({"ship_id": dict(item)["ship_id"]}, item, upsert=True)
        return item
