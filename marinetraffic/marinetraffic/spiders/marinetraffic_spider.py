import scrapy
import json
from marinetraffic.items import MarinetrafficItem


class MarinetrafficSpider(scrapy.Spider):
    name = "marine"
    headers = {'authority': 'www.marinetraffic.com',
               'method': 'GET', 'path': '/getData/get_data_json_4/',
               'scheme': 'https', 'accept': '/', 'accept-language': 'en,es;q=0.9',
               'cookie': '__cfduid=d6e6b3a4b2c1876a33f0f2246fee51eeb1546952553; _ga=GA1.2.354036928.1546952562; _pendo_visitorId.915388fa-afe0-454c-6270-7a41b245e92e=_PENDO_T_DP5lgmCeEwq; hubspotutk=c450c875b3b456259b77a4568ef25097; __zlcmid=qGhvUqk97fR6Ko; __atuvc=3%7C2; SERVERID=app2; _gid=GA1.2.1202160193.1547200550; CAKEPHP=olriaoblalruggbmvkiba97i04; __hssrc=1; _pendo_meta.915388fa-afe0-454c-6270-7a41b245e92e=1795342021; vTo=1; __hstc=153128807.c450c875b3b456259b77a4568ef25097.1546952587285.1547201498025.1547210182938.4; __hssc=153128807.2.1547210182938; _gat=1',
               'referer': 'https://www.marinetraffic.com/en/ais/home/centerx:72.0/centery:19.4/zoom:2',
               'user-agent': 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36',
               'vessel-image': '00784ab508cf1b47a58c62ad2a4926ebc421',
               'x-requested-with': 'XMLHttpRequest'}
    url = "https://www.marinetraffic.com/getData/get_data_json_4/z:{0}/X:{1}/Y:{2}/station:0"

    def start_requests(self):
        for x in range(0, 31):
            for y in range(0, 31):
                req = scrapy.Request(url=self.url.format(str(6), str(x), str(y)), callback=self.parse,
                                     headers=self.headers)
                yield req

    def parse(self, response):
        data = json.loads(response.body_as_unicode())
        for d in data["data"]["rows"]:
            itm = MarinetrafficItem()
            itm["shipname"] = d["SHIPNAME"] if "SHIPNAME" in d.keys() else None
            itm["lat"] = float(d["LAT"]) if "LAT" in d.keys() else None
            itm["lon"] = float(d["LON"]) if "LON" in d.keys() else None
            itm["speed"] = int(d["SPEED"]) if "SPEED" in d.keys() else None
            itm["destination"] = d["DESTINATION"] if "DESTINATION" in d.keys() else None
            itm["shipflag"] = d["FLAG"] if "FLAG" in d.keys() else None
            itm["shiptype"] = d["TYPE_NAME"] if "TYPE_NAME" in d.keys() else None
            itm["ship_id"] = d["SHIP_ID"] if "SHIP_ID" in d.keys() else None
            itm["length"] = int(d["LENGTH"]) if "LENGTH" in d.keys() else None
            itm["width"] = int(d["WIDTH"]) if "WIDTH" in d.keys() else None
            itm["dwt"] = int(d["DWT"]) if "DWT" in d.keys() else None
            itm["rot"] = d["ROT"] if "ROT" in d.keys() else None
            itm["l_fore"] = d["L_FORE"] if "L_FORE" in d.keys() else None
            itm["w_left"] = d["W_LEFT"] if "W_LEFT" in d.keys() else None


            # itm["shipflag"] = d["SHIPNAME"]
            # itm["shipflag"] = d["TYPE_NAME"]

            yield itm