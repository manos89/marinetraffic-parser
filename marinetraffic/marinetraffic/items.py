# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/items.html

import scrapy
from scrapy.loader.processors import TakeFirst

class MarinetrafficItem(scrapy.Item):
    shipname = scrapy.Field(output_processor=TakeFirst())
    lat = scrapy.Field(output_processor=TakeFirst())
    lon = scrapy.Field(output_processor=TakeFirst())
    speed = scrapy.Field(output_processor=TakeFirst())
    destination = scrapy.Field(output_processor=TakeFirst())
    shiptype = scrapy.Field(output_processor=TakeFirst())
    ship_id = scrapy.Field(output_processor=TakeFirst())
    shipflag = scrapy.Field(output_processor=TakeFirst())
    rot = scrapy.Field(output_processor=TakeFirst())
    length = scrapy.Field(output_processor=TakeFirst())
    width = scrapy.Field(output_processor=TakeFirst())
    dwt = scrapy.Field(output_processor=TakeFirst())
    l_fore = scrapy.Field(output_processor=TakeFirst())
    w_left = scrapy.Field(output_processor=TakeFirst())

